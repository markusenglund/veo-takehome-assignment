export type Node = {
  nodeId: string;
  name: string;
  height: number;
  department?: string;
  programmingLanguage?: string;
  parentId?: string;
  childIds: string[];
};
