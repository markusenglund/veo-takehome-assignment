import React, { useState } from "react";
import { Node } from "./types";

function NewNodeForm({
  selectedNodeId,
  addNodeToTree
}: {
  selectedNodeId: string | null;
  addNodeToTree: (node: Node) => void;
}) {
  const [formVisible, setFormVisible] = useState(false);
  function showForm() {
    setFormVisible(true);
  }

  async function handleSubmit(event: any) {
    event.preventDefault();

    const { elements } = event.target;
    const name = elements[0].value;
    const department = elements[1].value ?? null;
    const programmingLanguage = elements[2].value ?? null;

    const POST_URL = "http://localhost:3000/api/company-nodes";

    const response = await fetch(POST_URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name,
        department,
        programmingLanguage,
        parentId: selectedNodeId
      })
    });
    const { node } = await response.json();
    addNodeToTree(node);
    setFormVisible(false);
  }

  return (
    <div>
      <button onClick={showForm}>Create new node</button>
      {formVisible && (
        <form className="node-form" onSubmit={handleSubmit}>
          <div>
            <label>
              Name:
              <input type="text" name="name" required />
            </label>
          </div>
          <div>
            <label>
              Department:
              <input type="text" name="department" />
            </label>
          </div>
          <div>
            <label>
              Programming language:
              <input type="text" name="programmingLanguage" />
            </label>
          </div>
          <input type="submit" value="Submit" />
        </form>
      )}
    </div>
  );
}

export default NewNodeForm;
