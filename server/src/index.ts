import express from "express";
import companyNode from "./routes/companyNode";
import cors from "cors";
import { PORT } from "./constants";

const main = async () => {
  console.log("Initializing server...");

  const app = express();
  app.use(express.json());
  app.use(cors());
  app.use("/api/company-nodes", companyNode);

  app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
  });
};

main();
