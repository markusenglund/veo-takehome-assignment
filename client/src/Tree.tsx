import React from "react";
import { Node } from "./types";

function recursivelyRenderTree(
  treeRecord: Record<string, Node>,
  nodeId: string
): JSX.Element {
  const curNode = treeRecord[nodeId];
  return (
    <>
      <p key={curNode.nodeId} style={{ marginLeft: curNode.height * 22 }}>
        {curNode.name}
      </p>
      {curNode.childIds.map(childId =>
        recursivelyRenderTree(treeRecord, childId)
      )}
    </>
  );
}

function Tree({
  treeRecord,
  rootNodeId
}: {
  treeRecord: Record<string, Node>;
  rootNodeId: string | null;
  setSelectedNodeId: (nodeId: string) => void;
}) {
  if (!rootNodeId) {
    return <div>Tree is empty!</div>;
  }
  return (
    <div>
      <h1>Tree view</h1>
      {recursivelyRenderTree(treeRecord, rootNodeId)}
    </div>
  );
}

export default Tree;
