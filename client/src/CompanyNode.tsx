import React from "react";
import { Node } from "./types";

function CompanyNode({
  node,
  setSelectedNodeId
}: {
  node: Node;
  setSelectedNodeId: (nodeId: string) => void;
}) {
  return (
    <div>
      <h1>Current employee: {node.name}</h1>
      <p>ID: {node.nodeId}</p>
      <p>Height: {node.height}</p>
      <p>name: {node.name}</p>
      {node.department && <p>Department: {node.department}</p>}
      {node.programmingLanguage && <p>Language: {node.programmingLanguage}</p>}
      {node.parentId && (
        <p>
          <button onClick={() => setSelectedNodeId(node.parentId as string)}>
            Go to parent
          </button>
        </p>
      )}
      {node.childIds.length > 0 && (
        <p>
          Children:
          {node.childIds.map(id => (
            <button key={id} onClick={() => setSelectedNodeId(id)}>
              {id}
            </button>
          ))}
        </p>
      )}
    </div>
  );
}

export default CompanyNode;
