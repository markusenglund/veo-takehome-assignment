import React, { useEffect, useState } from "react";
import Tree from "./Tree";
import CompanyNode from "./CompanyNode";
import NewNodeForm from "./NewNodeForm";
import "./App.css";
import { Node } from "./types";

function App() {
  const [treeRecord, setTreeRecord] = useState<Record<string, Node>>({});
  const [rootNodeId, setRootNodeId] = useState<string | null>(null);
  const [selectedNodeId, setSelectedNodeId] = useState<string | null>(null);

  function addNodeToTree(node: Node) {
    const parentId = node.parentId;
    const newTreeRecord = {
      ...treeRecord,
      [node.nodeId]: node
    };

    if (parentId) {
      newTreeRecord[parentId].childIds.push(node.nodeId);
    }
    setTreeRecord(newTreeRecord);
    if (node.height === 0) {
      setRootNodeId(node.nodeId);
    }
    setSelectedNodeId(node.nodeId);
  }

  useEffect(() => {});

  return (
    <div className="App">
      <Tree
        treeRecord={treeRecord}
        rootNodeId={rootNodeId}
        setSelectedNodeId={setSelectedNodeId}
      />
      {treeRecord && selectedNodeId && (
        <CompanyNode
          node={treeRecord[selectedNodeId]}
          setSelectedNodeId={setSelectedNodeId}
        />
      )}
      <NewNodeForm
        selectedNodeId={selectedNodeId}
        addNodeToTree={addNodeToTree}
      />
    </div>
  );
}

export default App;
