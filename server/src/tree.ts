import crypto from "crypto";

export interface TreeNode {
  nodeId: string;
  name: string;
  height: number;
  department?: string;
  programmingLanguage?: string;
  parentId?: string;
  childIds: string[];
}
let root: TreeNode;

type TreeMap = Map<string, TreeNode>;
export const treeMap: TreeMap = new Map();

export function addNode(
  node: Pick<
    TreeNode,
    "name" | "department" | "programmingLanguage" | "parentId"
  >
): TreeNode {
  const nodeId = crypto.randomUUID();

  let parent: TreeNode | undefined;
  if (node.parentId) {
    parent = treeMap.get(node.parentId);
    if (!parent) {
      throw new Error("Couldn't find parent in tree");
    }
  }
  const height = parent ? parent.height + 1 : 0;

  const fullNode: TreeNode = {
    nodeId,
    name: node.name,
    height,
    department: node.department,
    programmingLanguage: node.programmingLanguage,
    parentId: node.parentId,
    childIds: []
  };

  if (parent) {
    parent.childIds.push(fullNode.nodeId);
  } else {
    root = fullNode;
  }

  treeMap.set(nodeId, fullNode);

  return fullNode;
}

export function getChildren(parentId: string): Omit<TreeNode, "children">[] {
  const parent = treeMap.get(parentId);
  if (!parent) {
    throw new Error("Couldn't find parent in tree");
  }

  const children = parent.childIds.map(nodeId => {
    const node = treeMap.get(nodeId) as TreeNode;
    return node;
  });

  return children;
}

export function changeParent(nodeId: string, newParentId: string): void {
  const node = treeMap.get(nodeId);
  if (!node) {
    throw new Error("Couldn't find node in tree");
  }

  if (!node.parentId) {
    throw new Error("Invalid operation: CEO can't be reassigned");
  }

  const prevParent = treeMap.get(node.parentId) as TreeNode;
  const newParent = treeMap.get(newParentId);
  if (!newParent) {
    throw new Error("Couldn't find parent in tree");
  }

  // Remove child
  const childIndex = prevParent.childIds.findIndex(
    childId => childId === nodeId
  );
  prevParent.childIds.splice(childIndex, 1);

  // Add child
  newParent.childIds.push(node.nodeId);
  node.parentId = newParent.nodeId;

  // Recursively fix height
  function recursivelyFixHeight(curNode: TreeNode, parentHeight: number): void {
    curNode.height = parentHeight + 1;
    for (const childId of curNode.childIds) {
      const childNode = treeMap.get(childId) as TreeNode;
      recursivelyFixHeight(childNode, curNode.height);
    }
  }

  recursivelyFixHeight(node, newParent.height);
}
