import { Router, Request } from "express";
import { TreeNode, addNode, getChildren, changeParent } from "../tree";

const router = Router();

type PartialNode = Pick<
  TreeNode,
  "name" | "department" | "programmingLanguage" | "parentId"
>;

router.post("/", async (req: Request<{}, {}, PartialNode>, res) => {
  const partialNode = req.body;

  const node = addNode(partialNode);

  return res.status(201).json({ node });
});

router.get(
  "/:nodeId/children",
  async (req: Request<{ nodeId: string }>, res) => {
    const { nodeId } = req.params;
    const children = getChildren(nodeId);
    res.json({ children });
  }
);

router.put(
  "/:nodeId/updateParent/:parentId",
  async (req: Request<{ nodeId: string; parentId: string }>, res) => {
    const { nodeId, parentId } = req.params;
    changeParent(nodeId, parentId);
    res.status(204).end();
  }
);

export default router;
